package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("/{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO groupchatDao = new GroupChatDAO();

        GroupChat groupchat = groupchatDao.getGroupChat(groupChatId);

        groupchat.setMessageList(groupchatDao.getGroupChatMessages(groupChatId));
        groupchat.setUserList(groupchatDao.getGroupChatUsers(groupChatId));


        return groupchat;
    }

    @GET
    @Path("/user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getPartOfConvo(@PathParam("userId") int userId){
        GroupChatDAO groupchatDao = new GroupChatDAO();
        return groupchatDao.getGroupChatByUserId(userId);
    }

    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupchat){
        GroupChatDAO groupchatDao = new GroupChatDAO();
        return groupchatDao.addGroupChat(groupchat);
    }

    @GET
    @Path("/{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> retreveMessages(@PathParam("groupChatId")int groupChatId){
        GroupChatDAO groupchatDao = new GroupChatDAO();
        return groupchatDao.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("/{groupChatId}/message")
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId")int groupChatId, Message message){
        GroupChatDAO groupchatDao = new GroupChatDAO();

        return groupchatDao.addMessage(groupChatId,message);
    }





}
